<header id="main-header">
    @section('nav')
        @include('lav-modules::includes.admin_navigation')
    @stop
    @yield('nav')
</header>