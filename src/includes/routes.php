<?php

//  Add Dashboard Route
Route::get(adminAlias(), array('as' => "dashboard", 'uses' => '\\Developeryamhi\\LaravelModules\\DashboardController@index'))->before('auth');

//  Add Manager Routes
Route::get(adminAliasPath() . "modules", array('as' => "modules", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@index'))->before('auth');
Route::get(adminAliasPath() . "add_module", array('as' => "add_module", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@add_module'))->before('auth');
Route::post(adminAliasPath() . "add_module_process", array('as' => "add_module_process", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@add_module_process'))->before('auth');
Route::get(adminAliasPath() . "scan_modules", array('as' => "scan_modules", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@scan_modules'))->before('auth');
Route::get(adminAliasPath() . "sync_modules", array('as' => "sync_modules", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@sync_modules'))->before('auth');
Route::get(adminAliasPath() . "activate_module/{id}", array('as' => "activate_module", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@activate_module'))->before('auth');
Route::get(adminAliasPath() . "force_activate_module/{id}", array('as' => "force_activate_module", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@force_activate_module'))->before('auth');
Route::get(adminAliasPath() . "deactivate_module/{id}", array('as' => "deactivate_module", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@deactivate_module'))->before('auth');
Route::get(adminAliasPath() . "delete_module/{id}", array('as' => "delete_module", 'uses' => '\\Developeryamhi\\LaravelModules\\ManagerController@delete_module'))->before('auth');