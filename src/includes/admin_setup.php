<?php

//  Load Start
require_once __DIR__ . '/start.php';

//  Check if Theme Manager is Available
if(!function_exists("setPageTitle"))    return;

//  Define Nav Parent
define("ADMIN_NAV_DASHBOARD", "dashboard");
define("ADMIN_NAV_ADMINISTRATION", "administration");
define("ADMIN_NAV_CONTENTS", "contents");
define("ADMIN_NAV_SYSTEM", "system");
define("ADMIN_NAV_SETTINGS", "settings");
define("ADMIN_NAV_FRONTEND", "front_end");

//  Listen Admin Controller Created Event
Event::listen("admin.controller.created", function() {

    //  Create Admin Nav Resource
    generate_navigation(adminNavGroup());

    //  Reset Resources
    resetResources(true);

    //  Change Base Page Title
    setBasePageTitle(app("config")->get("lav-modules::title"), true);

    //  Set Meta Data for Pages
    setRawMeta("charset", "UTF-8");
    setRawMeta("http-equiv", "X-UA-Compatible", array("content" => "IE=edge"));
    setMeta("viewport", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0");

    //  Enqueue Styles for Pages
    enqueue_style('jquery-ui', false, adminCssAssetURL('jquery/jquery-ui-1.10.3.min.css'));
    enqueue_style('bootstrap', false, adminCssAssetURL('bootstrap/bootstrap.min.css'));
    enqueue_style('bootstrap-tagsinput', false, adminCssAssetURL('bootstrap-tagsinput/bootstrap-tagsinput.css'));
    enqueue_style('bootstrap-switch', false, adminCssAssetURL('bootstrap/bootstrap-switch.min.css'));
    enqueue_style('bootstrap-wysihtml5', false, adminCssAssetURL('bootstrap-wysihtml5/bootstrap-wysihtml5.css'));
    enqueue_style('fancybox2', false, adminCssAssetURL('fancybox2/jquery.fancybox.css'));
    enqueue_style('font-awesome', false, adminCssAssetURL('font-awesome/font-awesome.css'));
    enqueue_style('spectrum', false, adminCssAssetURL('spectrum/spectrum.css'));
    enqueue_style('admin-theme', false, urlRoute(adminThemeUrlRoute()));
    enqueue_style('admin-style', false, urlRoute(adminStyleUrlRoute()));

    //  Enqueue Scripts for Pages
    enqueue_script('jquery', false, adminJsAssetURL('jquery/jquery-1.10.2.min.js'), VIEW_LOCATION_HEADER);
    enqueue_script('jquery-migrate', false, adminJsAssetURL('jquery/jquery-migrate.js'), VIEW_LOCATION_HEADER);
    enqueue_script('jquery-ui', false, adminJsAssetURL('jquery/jquery-ui-1.10.3.min.js'), VIEW_LOCATION_HEADER);
    enqueue_script('hashchange', false, adminJsAssetURL('hashchange/jquery.ba-hashchange.min.js'));
    enqueue_script('bootstrap', false, adminJsAssetURL('bootstrap/bootstrap.min.js'));
    enqueue_script('handlebars', false, adminJsAssetURL('bootstrap-typeahead/handlebars.js'));
    enqueue_script('typeahead-only', false, adminJsAssetURL('bootstrap-typeahead/typeahead_old.js'));
    enqueue_script('bootstrap-typeahead', false, adminJsAssetURL('bootstrap-typeahead/bootstrap.typeahead.js'));
    enqueue_script('bootstrap-tagsinput', false, adminJsAssetURL('bootstrap-tagsinput/bootstrap-tagsinput.js'));
    enqueue_script('bootstrap-switch', false, adminJsAssetURL('bootstrap/bootstrap-switch.min.js'));
    enqueue_script('bootstrap-prettyCheckable', false, adminJsAssetURL('bootstrap/prettyCheckable.min.js'));
    enqueue_script('wysihtml5', false, adminJsAssetURL('bootstrap-wysihtml5/wysihtml5-0.3.0.js'));
    enqueue_script('bootstrap-wysihtml5', false, adminJsAssetURL('bootstrap-wysihtml5/bootstrap-wysihtml5.js'));
    enqueue_script('jquery-validate', false, adminJsAssetURL('jquery-validate/jquery.validate.min.js'));
    enqueue_script('jquery-validate-extended', false, adminJsAssetURL('jquery.validate.extended.js'));
    enqueue_script('fancybox2', false, adminJsAssetURL('fancybox2/jquery.fancybox.pack.js'));
    enqueue_script('spectrum', false, adminJsAssetURL('spectrum/spectrum.js'));
    enqueue_script('custom_autocomplete', false, adminJsAssetURL('custom/jquery.e_autocomplete.js'));
    enqueue_script('tmpl', false, adminJsAssetURL('others/tmpl.min.js'));
    enqueue_script('admin-script', false, urlRoute(adminScriptUrlRoute()));

    //  Check Nav
    if($nav = nav(adminNavGroup())) {

        //  Add Dashboard Navigation Item
        $nav->addMenuItem(ADMIN_NAV_DASHBOARD, trans("lav-modules::menu_item.dashboard"), urlRoute("dashboard"), null, null, 10);

        //  Add Content Navigation Item
        $nav->addMenuItem(ADMIN_NAV_ADMINISTRATION, trans("lav-modules::menu_item.administration"), "#", null, null, 20);

        //  Add Content Navigation Item
        $nav->addMenuItem(ADMIN_NAV_CONTENTS, trans("lav-modules::menu_item.contents"), "#", null, null, 30);

        //  Add System Navigation Item
        $nav->addMenuItem(ADMIN_NAV_SYSTEM, trans("lav-modules::menu_item.system"), "#", null, null, 40);

        //  Add Settings Navigation Item
        $nav->addMenuItem(ADMIN_NAV_SETTINGS, trans("lav-modules::menu_item.settings"), "#", null, null, 50);

        //  Check Permissions
        if(isAdmin() || userHasPermission("manage_modules")) {

            //  Add Modules Manager Navigation
            $nav->addSubMenuItem(ADMIN_NAV_SYSTEM, "modules", trans("lav-modules::menu_item.modules"), urlRoute("modules"));
        }

        //  Check if Site Has Frontend
        if(urlRoute("dashboard") != urlRoute("home")) {

            //  Add Navigate to Frontend Navigation Item
            $nav->addRightMenuItem(ADMIN_NAV_FRONTEND, array("icon" => "glyphicon-log-out", "title" => trans("lav-modules::menu_item.front_end"), "append" => ' data-placement="bottom" target="_blank"'), urlRoute("home"), null, null, 5000);
        }
    }
});