<?php namespace Developeryamhi\LaravelModules\Commands;

class ModulesResetCommand extends AbstractCommand {

	/**
	 * Name of the command
	 * @var string
	 */
	protected $name = 'modules:reset';

	/**
	 * Command description
	 * @var string
	 */
	protected $description = 'Resets Modules Tables and Version Informations';

	/**
	 * Execute the console command.
	 * @return void
	 */
	public function fire()
	{

		//	Reset Everything
		app("lav-modules")->resetEverything();

		return $this->info("Modules informations has been successfully resetted");
	}

}
