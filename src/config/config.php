<?php

return array(

    //	Tables
    'table_modules' => 'modules',
    'table_module_versions' => 'module_versions',

    //	Modules Path
    'path' => 'app/modules',

    //	Module Meta File
    'meta_file' => 'module.json',

    //	Include Folder
    'includes_folder' => 'includes/',

    //  Preload Helper File
    'preload_helper' => 'helpers.php',

    //	Module Files to Include
    'include' => array(
        'bindings.php',
        'observers.php',
        'filters.php',
        'composers.php',
        'routes.php',
        'start.php'
    ),

    //	Default Handler File
    'handler_file' => 'module.php',

    //  Debug
    'debug' => false,

    /** Admin Settings **/

    //  Site Name
    'title' => 'Admin Panel',

    //  Top Title
    'top_title' => 'Administration',

    //  Footer Text
    'footer_text' => '&copy; ' . date("Y") . ' Admin Panel. All Rights Reserved.',

    //  Admin Prefix Alias
    'prefix_admin' => 'admin',

    //  Nav Group
    'nav_group' => 'admin_nav',

    //  Route Names
    'admin_theme_url_route' => 'admin_theme_url',
    'admin_style_url_route' => 'admin_style_url',
    'admin_script_url_route' => 'admin_script_url',
    'guest_theme_url_route' => 'guest_theme_url',
    'guest_style_url_route' => 'guest_style_url',
    'guest_script_url_route' => 'guest_script_url',
    'error_theme_url_route' => 'error_theme_url',
    'error_style_url_route' => 'error_style_url',
    'error_script_url_route' => 'error_script_url',

    //  Assets
    'public_assets' => true,
    'assets_folder' => null
);