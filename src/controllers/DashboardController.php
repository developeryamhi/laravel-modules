<?php namespace Developeryamhi\LaravelModules;

class DashboardController extends Base\AdminController {

    protected $namespaceName = "lav-modules";

    protected $controllerName = "dashboard";

    protected function init() {
        $this->layoutName = adminLayout();
    }

    public function index() {
        return $this->_render('dashboard');
    }
}
